<?php

/**
 * calcardbackup_wrapper.php - wrapper script to run calcardbackup without shell access
 * (enhanced version to also run occ commands without shell access)
 * Copyright (C) 2023 Bernhard Ostertag
 *
 * Source: https://codeberg.org/BernieO/calcardbackup
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */



/*****************************/
/******  CONFIGURATION  ******/
/*****************************/

/**
 * Give your calcardbackup or occ command  with the desired options below.
 * NOTE: the option '--no-interaction' will be added to occ-commands in any case by the script, as interaction is not possible.
 *
 * Examples (adapt paths to fit your needs):
 *
 * calcardbackup: $command = "./calcardbackup /var/www/nextcloud";
 *                $command = "/path/to/calcardbackup /var/www/nextcloud -bd /path/to/backup_dir -d _%d -ltm 30";
 * occ:           $command = "/var/www/nextcloud/occ list";
 *                $command = "../relative/path/to/nextcloud/occ files:scan --path=/alice/files/Music";
 */
$command = "./calcardbackup /PATH/TO/YOUR/NEXTCLOUD";



/*****************************/
/******  BEGIN SCRIPT   ******/
/*****************************/

// Script version:
$script_version = 'v6.0.0';

// function to check, whether a given function is enabled on this system:
function check_enabled ($function_name) {
	// returns true, if the given function is enabled and returns false if it is disabled
	if (! is_callable ($function_name)) {
		// function is not callable:
		return false;
	}
	$disabled_functions = explode (',', ini_get ('disable_functions'));
	foreach ($disabled_functions as $disabled_function) {
		if ($function_name === trim ($disabled_function)) {
			// function is disabled:
			return false;
		}
	}
	// function $function_name is enabled. Return true:
	return true;
}



///////////////////////////////////////////////////////////////////
// html HEAD:
echo '<!DOCTYPE html><html lang="en">' . PHP_EOL;
echo '<head>' . PHP_EOL;
echo '<title>calcardbackup wrapped in PHP with support for occ</title>' . PHP_EOL;
echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">' . PHP_EOL;
echo '<style>' . PHP_EOL;
echo ':root { --background: Beige; --text: Black; --link: Sienna; --hover: Royalblue; --good: LimeGreen; --bad: Red; }' . PHP_EOL;
echo 'a, a:visited { color: var(--link); text-decoration: none; border-bottom: 1px solid var(--link); }' . PHP_EOL;
echo 'a:hover, a:active { color: var(--hover); text-decoration: none; border-bottom: 1px solid var(--hover); }' . PHP_EOL;
echo 'body { color: var(--text); background-color: var(--background); font-family: sans-serif; margin: 1em; }' . PHP_EOL;
echo 'h1 > a { display: block; padding-bottom: 0.3em; font-size: 0.8em; }' . PHP_EOL;
echo 'pre { margin: 0em; }' . PHP_EOL;
echo '.enabled, .disabled { position: relative; top: 0.1em; }' . PHP_EOL;
echo '.enabled { color: var(--good); padding: 0em 0.2em 0em 1.5em; font-size: 2.0em; }' . PHP_EOL;
echo '.disabled { color: var(--bad); padding: 0em 0.4em 0em 2.4em; font-size: 1.3em; }' . PHP_EOL;
echo '.first { padding-left: 0em; }' . PHP_EOL;
echo '.function_to_be_used { font-weight: bold; color: var(--good); border-bottom: 1px solid var(--good); }' . PHP_EOL;
echo '.terminal_style { color: var(--text); background-color: white; padding: 0.5em; border: 1px solid var(--link); border-radius: 0.5em; overflow: auto; }' . PHP_EOL;
echo '</style>' . PHP_EOL;
echo '</head>' . PHP_EOL;


///////////////////////////////////////////////////////////////////
// html BODY:
echo '<body>' . PHP_EOL;
echo '<h1><a href="https://codeberg.org/BernieO/calcardbackup" target="_blank">calcardbackup_wrapper.php ' . $script_version . '</a></h3>' . PHP_EOL;
echo '<pre>' . PHP_EOL;

// print path to this file and configured $command:
echo 'Path to this file:' . PHP_EOL . htmlspecialchars ($_SERVER['SCRIPT_FILENAME']) . PHP_EOL . PHP_EOL;
echo 'configured command:' . PHP_EOL . $command . PHP_EOL;

// add interpreter to $command (bash for calcardbackup and php for occ):
if (preg_match ('/^[[:space:]]*[^[:space:]]*calcardbackup /', $command) === 1) {

	// calcardbackup:
	$command = '/usr/bin/env bash ' . $command;
	// remove donation line, as we print a link to the donations section in the repo later anyway:
	$pipe_sed = '| sed "/Support the developer/,+1d" 2>&1 ';
	$linefeed = '';

} else if (preg_match ('/^[[:space:]]*[^[:space:]]*occ /', $command) === 1) {

	// occ:
	// we need split the command to be able to add '--no-interaction' option (interaction ist not possible with a script like this):
	$command_left = substr ($command, 0, strpos ($command, ' '));
	$command_right = substr ($command, strpos ($command, ' '));
	$command = '/usr/bin/env php ' . $command_left . ' --no-interaction' . $command_right;
	$pipe_sed = '';
	// add an empty line for nicer output:
	$linefeed = '\n';

} else {

	// this script only executes 'calcardbackup' or 'occ'. Everything else is not supported:
	echo PHP_EOL . 'Only "calcardbackup" and "occ" are supported by this script.' . PHP_EOL;
	echo 'Sorry.</pre>' . PHP_EOL;
	echo '</body>' . PHP_EOL . '</html>';
	exit;

}

// redirect errors to stdout:
$command = $command . ' 2>&1 ';

echo PHP_EOL . 'Enabled PHP functions to run the command:' . PHP_EOL;
// go through all four possible functions to execute shell scripts in order of preference and assign $function_to_be_used
// passthru() has highest preference as it passes the output without interference to the browser window.
// shell_exec() has lowest preference, as it is the only function that does not pass the exit code of php.
$functions = array ('passthru', 'system', 'exec', 'shell_exec');

// assign class to first span-element, as we do not want a padding-left for it:
$first = ' first';

foreach ($functions as $function) {

	if (check_enabled ($function)) {
		if (! isset ($function_to_be_used)) {
			// assign $function_to_be_used if it is unset:
			$function_to_be_used = $function;
		}
		echo '<span class="enabled' . $first . '"' . $underline . '>&check;</span>' . $function . '()';
	} else {
		echo '<span class="disabled' . $first . '">&Chi;</span>' . $function . '()';
	}
	// clear class for first span-element as we continue with second span-element:
	$first = '';
}
echo PHP_EOL . PHP_EOL;

// if $function_to_be_used is still unset, there was no function found that is usable to run shell commands:
if (! isset ($function_to_be_used)) {
	// there is no function enabled to run shell scripts on this system:
	echo 'To run shell scripts, either one of those functions has to be enabled.' . PHP_EOL;
	echo 'But all four PHP functions are disabled on this system. Thus I am not able to run the command.' . PHP_EOL;
	echo 'Sorry.</pre>' . PHP_EOL;
	echo '</body>' . PHP_EOL . '</html>';
	exit;
}

// print function that will be used and the according complete command:
echo 'using <span class="function_to_be_used">' . $function_to_be_used . '()</span> to run the command:' . PHP_EOL . PHP_EOL;

// add 'header' and 'footer' to the command for a nicer shell output:
$command = 'printf "%s' . $linefeed . '\n" ">>> ' . $command . '"; ' . $command . $pipe_sed . '; printf "' . $linefeed . '%s\n" ">>> Command executed. Exitcode: ${?}"';

// terminal style div block for output:
echo '<div class="terminal_style">';

// run the command with the function which is set in $function_to_be_used:
switch ($function_to_be_used) {

	case 'passthru':
		passthru ($command, $exitcode);
		break;

	case 'system':
		system ($command, $exitcode);
		break;

	case 'exec':
		exec ($command, $output, $exitcode);
		// exec() returns an array with lines. Go through lines and print them:
		foreach ($output as $line) {
			echo $line . PHP_EOL;
		}
		break;

	case 'shell_exec':
		$output = shell_exec ($command);
		// print the output of the command which is stored in $output:
		echo $output;
		break;

}

// close terminal style div block:
echo '</div>' . PHP_EOL;

// add link to donations section in the repo and close <pre> tag:
echo 'Support the developer:' . PHP_EOL;
echo '<a href="https://codeberg.org/BernieO/calcardbackup#donations" target="_blank">https://codeberg.org/BernieO/calcardbackup#donations</a>' . PHP_EOL;
echo PHP_EOL . '</pre>' . PHP_EOL;

// print html footer and exit:
echo '</body>' . PHP_EOL . '</html>';
exit;

?>
