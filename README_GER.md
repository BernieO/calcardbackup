# calcardbackup

:gb: [read this in english...](README.md)

Dieses Bash-Skript exportiert Kalender und Adressbücher aus ownCloud/Nextcloud als .ics- und .vcf-Dateien und speichert sie in einem komprimierten Archiv. Weitere Optionen stehen zur Verfügung.

## Inhalt
- [Voraussetzungen](#voraussetzungen)
- [Schnellinstallation](#schnellinstallation)
- [*calcardbackup* aktualisieren](#calcardbackup-aktualisieren)
- [Optionen](#optionen)
- [Dateinamenkonvention](#dateinamenkonvention)
- [Beispiele](#beispiele)
- [Nextcloud-Snap Benutzer](#nextcloud-snap-benutzer)
- [Syncloud Benutzer](#syncloud-benutzer)
- [Synology Benutzer](#synology-benutzer)
- [Funktioniert das auch mit einer kaputten ownCloud/Nextcloud Installation?](#funktioniert-das-auch-mit-einer-kaputten-owncloud-nextcloud-installation)
- [Kann ich das auch ohne Shell Zugang zu meinem Server benutzen?](#kann-ich-das-auch-ohne-shell-zugang-zu-meinem-server-benutzen)
- [Erwähnenswertes zur Verschlüsselungsoption](#erwähnenswertes-zur-verschlüsselungsoption)
- [Links](#links)
- [Kontakt](#kontakt)
- [Spenden](#spenden)
- [Lizenz](#lizenz)

## Voraussetzungen

- lokale Installation von ownCloud/Nextcloud >= 5.0 mit MySQL/MariaDB, PostgreSQL oder SQLite3
- der zur Datenbank gehörende command line client
- der das Skript startende User muss Leserechte für den gesamten Pfad zu ownClouds/Nextclouds `config.php`, zum Skript selbst und zu allen benutzten Konfigurationsdateien haben
- GNU Bash >= 4.2 (prüfen mit `bash --version`)
- *optional*: das Paket `gnupg`, um Backups zu verschlüsseln
- *optional*: das Paket `zip`, um Backups als zip-Datei zu komprimieren (anstelle tar.gz)
- *optional*: das Paket `curl`, um zusätzliche Prüfungen auszuführen

## Schnellinstallation
1. das Repository auf Ihren Server klonen (nicht ins webroot!) und ins Verzeichnis wechseln:  
`git clone https://codeberg.org/BernieO/calcardbackup.git`  
`cd calcardbackup`

2. die Besitzrechte des Repository dem Webserver-User zuweisen (hier `www-data`):  
`sudo chown -R www-data:www-data .`

3. das Skript als Webserver-User (hier `www-data`) aufrufen und als Argument den Pfad zu ownCloud/Nextcloud übergeben (hier `/var/www/nextcloud`):  
`sudo -u www-data ./calcardbackup "/var/www/nextcloud"`

4. die Ausgabe des Skripts beobachten. Falls weitere Optionen benötigt werden, wird dies ausgegeben.

5. Das Backup befindet sich im Verzeichnis `backups/`.

Es gibt viele weitere Optionen, die dem Skript übergeben werden können (siehe [Optionen](#optionen) und [Beispiele](#beispiele)).

#### optional: automatische, tägliche Ausführung durch Erstellen eines Cronjobs

Wenn das Skript fehlerfrei läuft, empfiehlt es sich, die Ausführung zu automatisieren.  
Für den täglichen Aufruf kann folgendermaßen ein Cronjob erstellt werden:

1. zunächst eine Logdatei erstellen und die Besitzrechte dem Webserver-User (hier `www-data`) zuweisen:  
   `sudo touch /var/log/calcardbackup.log`  
   `sudo chown www-data:www-data /var/log/calcardbackup.log`

2. die Cron-Tabelle des Webserver-Users (hier `www-data`) zum Bearbeiten öffnen mit:  
   `sudo crontab -u www-data -e`  
   und am Ende der Datei die folgende Zeile hinzufügen (Pfade anpassen!):
   ```text
   0 2 * * * /pfad/zu/calcardbackup/calcardbackup "/var/www/nextcloud" > /var/log/calcardbackup.log 2>&1
   ```

Nun wird Cron *calcardbackup* jeden Tag um 02:00 Uhr morgens aufrufen.  
Die Skriptausgabe der jeweils letzten Ausführung befindet sich in der Logdatei `/var/log/calcardbackup.log`.

Es sollte beachtet werden, dass Prozent-Zeichen (`%`) in einer crontab-Datei in Zeilenumbrüche umgewandelt werden, sofern sie nicht maskiert sind.

## *calcardbackup* aktualisieren

Wenn *calcardbackup* nach der [Schnellinstallation](#schnellinstallation) eingerichtet wurde, genügt ein `git pull` im Installationsordner, um das Skript zu aktualisieren:
```text
cd /path/to/calcardbackup
sudo -u www-data git pull
```

## Optionen
Alle Optionen können als Konfigurationsdatei oder über die Kommandozeile übergeben werden. Ohne Optionen, oder nur mit Option `-b|--batch` aufgerufen, benutzt das Skript die Datei `calcardbackup.conf` im Skriptverzeichnis als Konfigurationsdatei, sofern vorhanden.  
Falls keine Konfigurationsdatei über die Option `-c|--configfile` an das Skript übergeben wird, muss der Pfad zur ownCloud/Nextcloud Instanz als erstes Argument übergeben werden.  
Im Folgenden werden die verfügbaren Optionen ausführlich beschrieben:

```text
Aufruf: ./calcardbackup [VERZEICHNIS] [Option [Argument]] [Option [Argument]] [Option [Argument]] ...

Argumente in Großbuchstaben sind für die jeweiligen Optionen notwendig.
Pfade (DATEI / VERZEICHNIS) sind absolute Pfade oder relative Pfade zum Arbeitsverzeichnis.
Falls ein Pfad mit einer Tilde-Slash Kombination ~/ beginnt, wird die Tilde durch ${HOME} ersetzt.

-a | --address URL
       Optional: übergibt die URL der ownCloud/Nextcloud Installation an das Skript.
-b | --batch
       Batchmodus: außer des Pfades zum Backup erfolgt keine Ausgabe.
       Abhängig von der Konfiguration ist das:
          - absoluter Pfad zum komprimierten Backup-Archiv
       oder, falls mit Option '-x|--uncompressed' aufgerufen,
          - absoluter Pfad zum Verzeichnis mit den unkomprimierten Backupdateien
-c | --configfile DATEI
       Benutze DATEI als Konfigurationsdatei. Siehe 'examples/calcardbackup.conf.example'
       Weitere angegebene Optionen mit Ausnahme von '-b|--batch' werden ignoriert!
-d | --date FORMAT
       Benutze FORMAT als Dateinamenserweiterung für die komprimierte Backupdatei oder das
       Backupverzeichnis. FORMAT muss eine gültige Formatbeschreibung des Befehls date() sein.
       Als Standard wird -%Y-%m-%d benutzt, was zu folgender Datei- oder Verzeichnisbenennung führt:
       'calcardbackup-2017-03-23.tar.gz' oder 'calcardbackup-2017-03-23'
       Informationen über verschiedene Formate und die Syntax finden sich unter 'man date'
-e | --encrypt DATEI
       Verschlüssele das komprimierte Backup mit AES256 (gnupg).
       Die erste Zeile von DATEI wird als Passphrase benutzt werden.
-h | --help
       Gib Versionsnummer und einen kurzen Hilfetext aus
-i | --include-shares
       Sichere auch geteilte Adressbücher und Kalender, aber nur einmal: z.B. ein geteilter
       Kalender wird nicht erneut gesichert, wenn derselbe Kalender schon für einen anderen
       Nutzer gesichert wurde.
       ACHTUNG: diese Option wird ignoriert, wenn nicht in Kombination mit '-u|--usersfile' benutzt.
-ltm | --like-time-machine N
       Behalte alle Sicherungen der letzten N Tage, für die Zeit davor aber nur solche, die Montags
       erstellt wurden.
-m | --mask N
       Benutze N als Dateimodus-Erstellungsmaske (umask) für alle Dateien, die erstellt werden.
       N muss eine vierstellige Oktalzahl sein. Führende Nullen können weggelassen werden.
-na | --no-addressbooks
       Sichere keine Adressbücher
-nc | --no-calendars
       Sichere keine Kalender
-o | --output VERZEICHNIS
       Benutze VERZEICHNIS, um die Backups zu speichern.
       Fehlt diese Option, wird das Verzeichnis 'backups/' im Skriptverzeichnis erstellt und benutzt.
-one | --one-file-per-component
       Speichere jede Kalender-Komponente (z.B. Event) und jede Addressbuch-Komponente in eine einzelne
       Datei mit Namen USERNAME-(CALENDARNAME|ADDRESSBOOKNAME)_UID.(ics|vcf).
       In diesem Modus nimmt calcardbackup keine Änderungen an den von der Datenbank erhaltenen Daten
       vor, außer die Zeilenenden mit CR+LF zu gestalten (wie in RFC5545/RFC6350 vorgeschrieben).
       Mit Hilfe dieser Option können fehlerhafte Datenbankeinträge untersucht oder
       Kalender/Addressbücher zu einem Radicale caldav/carddav Server oder zu vdirsyncer migriert
       werden.
-p | --snap
       Diese Option ist verpflichtend bei nextcloud-snap (https://github.com/nextcloud/nextcloud-snap).
       calcardbackup muss in diesem Fall mit sudo gestartet werden (ausgeführt als root ohne sudo wird
       auch scheitern!).
-r | --remove N
       Lösche Backups älter als N Tage vom Backupverzeichnis (N muss eine positive Ganzzahl sein).
-rmo | --read-mysql-optionfiles
       Lies alle MySQL/MariaDB Optionsdateien ein (bei Verbindungen zu einer MySQL/MariaDB Datenbank)
       ACHTUNG: wenn calcardbackup keine Dateidescriptoren benutzen kann, werden sowieso
                alle MySQL/MariaDB Optionsdateien eingelesen.
-s | --selfsigned
       Ignoriere ein nicht vertrauenswürdiges (z.B. selbstsigniertes) Zertifikat. cURL wird
       benutzt, um status.php abzurufen und damit zusätzliche Prüfungen durchzuführen. Falls
       cURL die URL aufgrund eines nicht vertrauenswürdigen Zertifikates nicht erreichen kann,
       wird calcardbackup diese zusätzlichen Prüfungen überspringen. Dasselbe gilt, falls cURL
       nicht installiert ist.
-t | --temporary-directory VERZEICHNIS
       Erstelle das Arbeitsverzeichnis für temporäre Dateien in VERZEICHNIS
-u | —usersfile DATEI
       Benutze DATEI, die die Nutzernamen der Nutzer enthält, deren Daten gesichert werden sollen.
       Ein Nutzer pro Zeile. Siehe 'examples/users.txt.example'
-x | --uncompressed
       komprimiere das Backup nicht
-z | --zip
       Benutze zip, um das Backup zu komprimieren, an Stelle eines gzip-tar-Archivs (tar.gz)

ACHTUNG: - die Option '-f|--fetch-from-database' (eingeführt mit calcardbackup 0.6.0) ist
           der Standard für calcardbackup >= 0.8.0 und hat daher keine Funktion mehr.
         - die Option '-g|--get-via-http' wurde mit v2.0.0 entfernt (veraltet seit v0.8.0, 30.10.2018).
```

## Dateinamenkonvention

Die Dateinamen der exportierten Kalender und Adressbücher werden zusammengesetzt aus dem Nutzernamen, dem Namen des Kalenders/Adressbuchs und einer Dateierweiterung (`.ics` für Kalender, `.webcal` für Kalenderabonnements, `.vcf` für Adressbücher):
```
Tom_Konferenzen.ics
Tom_Bundesliga.webcal
Tom_Kontakte.vcf
```
In folgenden Fällen wird dem Dateinamen eine zusätzliche Bezeichnung hinzugefügt (Kombinationen sind möglich):
- vorhandene Dateien werden niemals überschrieben. Stattdessen wird dem Dateinamen eine Zahl direkt vor der Dateinamenserweiterung hinzugefügt:
  ```
  Tom_Konferenzen_01.ics
  ```
- beim Benutzen der Option `-i|--include-shares` wird `_shared-by-USERNAME` zum Dateinamen von geteilten Kalendern/Adressbüchern hinzugefügt (Tom teilt seinen Kalender *Konferenzen* mit Emma):
  ```
  Emma_Konferenzen_shared-by-Tom.ics
  ```
- beim Benutzen der Option `-one|--one-file-per-component` wird die UID des betreffenden Elementes dem Dateinamen hinzugefügt:
  ```
  Tom_Konferenzen_1234-5678.ics
  ```
- nur Nextcloud >= 22: dem Dateinamen eines gelöschten Kalenders wird `_DEL-CLNDR` hinzugefügt:
  ```
  Tom_Konferenzen_DEL-CLNDR.ics
  ```
- nur Nextcloud >= 22: falls ein Kalender gelöschte Elemente enthält, werden diese gelöschten Elemente in einem separaten Kalender gesichert. Dem Dateinamen wird `_DEL-CMPNTS` hinzugefügt:
  ```
  Tom_Konferenzen_DEL-CMPNTS.ics
  ```

## Beispiele

1. `./calcardbackup /var/www/nextcloud -nc -x`  
Sichere keine Kalender (`-nc`) und speichere das Backup unkomprimiert (`-x`) im Verzeichnis `calcardbackup-YYYY-MM-DD` (Standard) in `./backups/` (Standard).

2. `./calcardbackup /var/www/nextcloud --no-calendars --uncompressed`  
Dies ist genau dasselbe Kommando wie im ersten Beispiel, allerdings mit langen Optionsnamen statt der kurzen.

3. `./calcardbackup -c /etc/calcardbackup.conf`  
Benutze die Konfigurationsdatei /etc/calcardbackup.conf (`-c /etc/calcardbackup.conf`). Alle Parameter für das gewünschte Verhalten müssen in dieser Datei angegeben werden (siehe [examples/calcardbackup.conf.example](examples/calcardbackup.conf.example)).  
Die Angabe weiterer Optionen entfällt, da sie ignoriert werden (mit Ausnahme von `-b|--batch`).

4. `./calcardbackup`  
Benutze die Datei calcardbackup.conf im Verzeichnis des Skripts als Konfigurationsdatei.  
Dies ist im Grunde dasselbe wie Beispiel Nr. 3, nur mit dem Standardpfad der Konfigurationsdatei.

5. `./calcardbackup /var/www/nextcloud -b -d .%d.%H -z -e /home/tom/key -o /media/data/backupfolder/ -u /etc/calcardbackupusers -i -m 0077 -r 15`  
Unterdrücke alle Ausgaben außer dem Pfad zum Backup (`-b`), benutze die Dateinamenserweiterung .DD.HH (`-d .%d.%H`), benutze zip, um das Backup zu komprimieren (`-z`), verschlüssele das komprimierte Backup und benutze als Schlüssel die erste Zeile der Datei /home/tom/key (`-e /home/tom/key`), speichere das Backup im Verzeichnis /media/data/backupfolder/ (`-o /media/data/backupfolder/`), sichere nur Elemente von in der Datei /etc/calcardbackupusers angegebenen Nutzern (`-u /etc/calcardbackupusers`), schließe mit Nutzern geteilte Adressbücher/Kalender mit ein (`-i`), benutze 0077 als Dateimodus-Erstellungsmaske (`-m 0077`) und lösche alle Sicherungen älter als 15 Tage (`-r 15`).

6. `sudo ./calcardbackup /var/snap/nextcloud/current/nextcloud -p`  
Dieses Beispiel ist für Nutzer von [nextcloud-snap](https://github.com/nextcloud/nextcloud-snap). *calcardbackup* wird die Dienstprogramme von nextcloud-snap benutzen (`-p`), um alle in der Datenbank vorhandenen Kalender und Adressbücher zu sichern.

7. `./calcardbackup /var/www/nextcloud -ltm 30 -r 180`  
Behalte alle Sicherungen der letzten 30 Tage, für die Zeit davor aber nur solche, die Montags erstellt wurden (`-ltm 30`) und lösche alle Backups, die älter als 180 Tage sind (`-r 180`).  
:warning: Es muss sichergestellt werden, dass Backups auch Montags erstellt werden, wenn die Option `-ltm` benutzt wird.

8. `./calcardbackup /var/www/nextcloud -o /mnt/samba/share -t /var/tmp`  
Speichere das Backup im Ordner /mnt/samba/share (`-o /mnt/samba/share`) und erstelle das Arbeitsverzeichnis für temporäre Dateien in /var/tmp (`-t /var/tmp`).  
:point_right: Wenn sich das Ausgabeverzeichnis auf einer Netzwerkfreigabe befindet, kann eine lange Skriptlaufzeit mit der Option `-t|--temporary-directory` vermieden werden.

## Nextcloud-Snap Benutzer

Falls [Nextcloud-Snap](https://github.com/nextcloud/nextcloud-snap) benutzt wird, muss das Skript mit Option `-p|--snap` aufgerufen werden. *calcardbackup* wird dann das im Snap-Paket enthaltene Dienstprogramm `nextcloud.mysql-client` benutzen, um auf die Datenbank zuzugreifen.  
Damit dies funktioniert, muss *calcardbackup* mit `sudo` aufgerufen werden (als root ohne `sudo` aufgerufen wird auch fehlschlagen).  
Als Pfad zu Nextcloud muss der Pfad zu den Konfigurationsdateien des Snap Paketes angegeben werden. Bei einer Standardinstallation ist dies `/var/snap/nextcloud/current/nextcloud`. Siehe [Beispiel Nr. 6](#beispiele).

## Syncloud Benutzer

Unter [syncloud](https://www.syncloud.org) muss das Skript als User `nextcloud` mit der Option `-p` aufgerufen werden. Zusätzlich muss beim Aufruf von *calcardbackup* der Pfad zu den snap-binaries der Pfad-Variablen hinzugefügt werden:
```text
sudo -u nextcloud PATH="${PATH}:/snap/bin" ./calcardbackup "/var/snap/nextcloud/current/nextcloud" -p
```
Es gibt auch ein kleines Code-Beispiel über die Benutzung von *calcardbackup* [im Wiki der Syncloud-Plattform](https://github.com/syncloud/platform/wiki/Nextcloud#export-calendarscontacts). 

## Synology Benutzer

Beim Synology DiskStation Manager (DSM) muss vor Aufruf von *calcardbackup* der Pfad zu `mysql` der `PATH` Variablen hinzugefügt werden. Beispiel:
```text
sudo -u http PATH="${PATH}:/usr/local/mariadb10/bin" ./calcardbackup "/volume1/web/nextcloud"
```

## Funktioniert das auch mit einer kaputten ownCloud/Nextcloud Installation?

__Ja, das geht!__ :smiley:

*calcardbackup* benötigt lediglich die Datenbank (und Zugang zu ihr) einer ownCloud/Nextcloud Installation, um Kalender/Adressbücher von der Datenbank auszulesen und sie als .ics und .vcf Datein zu sichern (ein Datenbank-dump muss zuerst in eine Datenbank importiert werden).  
So kann vorgegangen werden:

1. eine Nextcloud Verzeichnis Attrappe anlegen (inklusive Unterverzeichnis `config`):  
`mkdir -p /usr/local/bin/nextcloud_dummy/config`

2. eine Datei `config.php` anlegen und mit folgenden Werten füllen:  
`nano /usr/local/bin/nextcloud_dummy/config/config.php`

    - den Typ der Datenbank wie in [config.sample.php](https://github.com/nextcloud/server/blob/v20.0.8/config/config.sample.php#L94-L105)

    - für MySQL/MariaDB/PostgreSQL:
      - die entsprechenden Datenbankwerte wie in [config.sample.php](https://github.com/nextcloud/server/blob/v20.0.8/config/config.sample.php#L107-L139)
    - für SQLite3:
      - den Pfad des in Schritt 1 angelegten nextcloud_dummy Verzeichnisses als 'datadirectory' wie in [config.sample.php](https://github.com/nextcloud/server/blob/v20.0.8/config/config.sample.php#L80-L86)
      - die SQLite3 Datenbank in die Verzeichnisattrappe kopieren (der Dateiname der SQLite3 Datenbank muss `owncloud.db` lauten):  
      `cp /path/to/owncloud.db /usr/local/bin/nextcloud_dummy/owncloud.db`

    - falls die Datenbank zu einer Installation von ownCloud <= 8.2 gehört, muss folgende Zeile hinzugefügt werden:  
      `'version' => '8.0.0',`

3. *calcardbackup* ausführen und als erstes Argument den Pfad zu der in Schritt 1 angelegten Nextcloud Verzeichnisattrappe angeben:  
`./calcardbackup /usr/local/bin/nextcloud_dummy`

## Kann ich das auch ohne Shell Zugang zu meinem Server benutzen?

Abhängig von der Konfiguration des Webhosters kann *calcardbackup* eventuell auch ohne Shell-Zugang benutzt werden: Zusätzlich zu den Voraussetzungen des Skriptes muss der Hoster zulassen, dass eine PHP-Funktion Shell-Skripte ausführen darf (entweder `passthru()`, `system()`, `exec()` oder `shell_exec()`). Nur dann kann *calcardbackup* ausgeführt werden, indem es vom im Repo enthaltenen [`calcardbackup_wrapper.php`](calcardbackup_wrapper.php) *calcardbackup* aufgerufen wird.  
Es wird folgendermaßen genutzt:

1. [`calcardbackup`](calcardbackup) und [`calcardbackup_wrapper.php`](calcardbackup_wrapper.php) in ein Verzeichnis auf dem Webserver hochladen

2. die Pfade des Kommandos [im Konfigurationsbereich von `calcardbackup_wrapper.php`](calcardbackup_wrapper.php#L26-L41) entsprechend der Umgebung anpassen und nach Bedarf weitere Optionen hinzufügen.

3. mit dem Browser die gerade hochgeladene Datei `calcardbackup_wrapper.php` aufrufen und die Ausgabe überprüfen.

Da es sehr viele unterschiedliche Konfigurationen bei den Webhostern gibt, kann es vorkommen, dass *calcardbackup* nicht ausgeführt werden kann oder nicht alle Optionen funktionieren werden (manche Hoster deaktivieren aus Sicherheitsgründen die benötigten PHP-Funktionen, andere mögen sie aktiviert haben, sperren aber den Aufruf von externen Befehlen). Das Kommando, um *calcardbackup* aufzurufen, muss also eventuell angepasst oder um verschiedene Optionen ergänzt werden.

Bei einer sehr großen zu sichernden Datenmenge können PHP-Timeouts auftreten. Diese können umgangen werden, indem das Skript zweimal ausgeführt wird (einmal mit der Option `-na`, um nur die Kalender zu sichern, ein zweites Mal mit der Option `-nc` um nur Adressbücher zu sichern). Eine andere Möglichkeit ist, bei jedem Skriptdurchlauf nur einen Teil der Nutzerdaten zu sichern (über die Option `-u`).

:warning: Um ein unberechtiges Herunterladen der gesicherten Daten zu verhindern, sollte der Zugang zum Backup-Ordner mit einem Passwort geschützt werden (z.B. über eine `.htaccess`-Datei).

:bangbang: *calcardbackup_wrapper.php* kann auch [Nextclouds *occ* Kommando](https://docs.nextcloud.com/server/stable/admin_manual/configuration_server/occ_command.html) aufrufen. Beispiele hierfür finden sich [im Konfigurationsbereich von `calcardbackup_wrapper.php`](calcardbackup_wrapper.php#L26-L41).

Ich freue mich über [Feedback](https://codeberg.org/BernieO/calcardbackup/issues/41), mit welchen Hostern das funktioniert oder nicht funktioniert.

## Erwähnenswertes zur Verschlüsselungsoption

Falls die Verschlüsselungsoption des Skripts benutzt wird (Option `-e`), sollte man sich folgender Dinge bewusst sein:
- die Dateien werden von [GnuPG](https://de.wikipedia.org/wiki/GNU_Privacy_Guard) ([AES256](https://de.wikipedia.org/wiki/Advanced_Encryption_Standard)) mit einem Passwort verschlüsselt, das in einer separaten Datei angegeben wird
- das Passwort ist in einer Datei gespeichert. Andere Nutzer mit Zugang zum Server könnten das Passwort einsehen.
- *calcardbackup* soll ohne Benutzerinteraktion funktionieren. Daher kann es keine bombensichere Verschlüsselung anbieten. Ich betrachte die angebotene Verschlüsselungsmöglichkeit allerdings für die meisten Anwendungsfälle als ausreichend
- falls bombensichere Verschlüsselung benötigt wird, lassen Sie nicht *calcardbackup* die Sicherung verschlüsseln. Verschlüsseln Sie das Archiv stattdessen selbst.
- das Kommando zum Entschlüsseln lautet (nach Eingabe des Befehls wird das Passwort angefordert):  
`gpg -o AUSGABE_DATEI -d VERSCHLÜSSELTE_DATEI.GPG`

## Links

#### Zugehörige Forenthreads
- [help.nextcloud.com](https://help.nextcloud.com/t/calcardbackup-bash-script-to-backup-nextcloud-calendars-and-addressbooks-as-ics-vcf-files/11978) - Nextcloud
- [central.owncloud.org](https://central.owncloud.org/t/calcardbackup-bash-script-to-backup-owncloud-calendars-and-addressbooks-as-ics-vcf-files/7340) - ownCloud

#### Blogartikel über *calcardbackup*
- [testorakel.de](https://testorakel.de/posts/2409-kein-backup-kein-mitleid/#nextcloud) - September 2024 (deutsch)
- [tuxwise.net](https://tuxwise.net/all-episodes/s02e01-archive-this/#Calendars_and_address_books) - Februar 2022 (englisch)
- [strobelstefan.org](https://strobelstefan.de/blog/2020/12/01/nextcloud-datensicherung-von-kalendern-und-adressbuechern-automatisch-erstellen/) - Dezember 2020 (deutsch)
- [www.patrickweber.info](https://www.patrickweber.info/verschlusseltes-backup-fur-nextcloud-kontakte/) - August 2020 (deutsch)
- [Neil's Tech Docs](https://neilstechdocs.blogspot.com/2020/04/nextcloud-addresscalendar-bbackups.html) - April 2020 (englisch)
- [newtoypia.blogspot.com](https://newtoypia.blogspot.com/2019/04/nextcloud.html) - April 2019 (taiwanisch)
- [bob.gatsmas.de](https://bob.gatsmas.de/calcardbackup-kalender-und-adressbuchbackup-von-owncloud-nextcloud) - April 2017 (deutsch)

#### *calcardbackup* in der Presse
- [Linux Magazine (235)](https://www.linux-magazine.com/Issues/2020/235/Using-CalDAV-CardDAV-to-manage-calendars-and-address-books/(offset)/3#eztoc0_0_2) - Juni 2020 (englisch)

#### Linux Pakete
- [aur.archlinux.org](https://aur.archlinux.org/packages/calcardbackup/) - *calcardbackup* im Arch User Repository ([wiki](https://wiki.archlinux.org/title/Nextcloud#Backup_calendars_and_address_books_with_calcardbackup))

#### Docker Abbild für *calcardbackup*
- [hub.docker.com](https://hub.docker.com/r/waja/calcardbackup) - Docker Abbild von waja

#### Nextcloud All-in-One Community Container
- [github.com](https://github.com/nextcloud/all-in-one/tree/main/community-containers/calcardbackup) - Nextcloud AIO Community Container für *calcardbackup*

#### ICS und VCF Standard
- [RFC 5545](https://tools.ietf.org/html/rfc5545) - Internet Calendaring and Scheduling Core Object Specification (iCalendar)
- [RFC 6350](https://tools.ietf.org/html/rfc6350) - vCard Format Specification

#### Exporter Plugins von SabreDAV
- [ICSExportPlugin.php](https://github.com/sabre-io/dav/blob/master/lib/CalDAV/ICSExportPlugin.php) - ICS Exporter `public function mergeObjects`
- [VCFExportPlugin.php](https://github.com/sabre-io/dav/blob/master/lib/CardDAV/VCFExportPlugin.php) - VCF Exporter `public function generateVCF`

## Kontakt

[Eröffne ein Issue](https://codeberg.org/BernieO/calcardbackup/issues/new), kommentiere meinen [Blogbeitrag über *calcardbackup*](https://bob.gatsmas.de/calcardbackup-kalender-und-adressbuchbackup-von-owncloud-nextcloud) oder schreibe mir eine [E-Mail](mailto:bernieo.code@gmx.de).

## Spenden

*calcardbackup* ist und bleibt freie Open Source Software.  
Falls dir *calcardbackup* gefällt und du das Skript nützlich findest, könntest du den Entwickler unterstützen und eine Spende tätigen. Jeder Betrag ist willkommen.

- IBAN: `DE58 1203 0000 1032 8146 40` (BIC: `BYLADEM1001`)
- PayPal (Freunde): `bernieo.code@gmx.de`

## Lizenz

Copyright (C) 2017 Bernhard Ostertag [bernieo.code@gmx.de](mailto:bernieo.code@gmx.de)
```
Dieses Programm ist freie Software. Sie können es unter den Bedingungen der
GNU Affero General Public License, wie von der Free Software Foundation veröffentlicht,
weitergeben und/oder modifizieren, entweder gemäß Version 3 der Lizenz oder (nach
Ihrer Option) jeder späteren Version.

Die Veröffentlichung dieses Programms erfolgt in der Hoffnung, daß es Ihnen von Nutzen
sein wird, aber OHNE IRGENDEINE GARANTIE, sogar ohne die implizite Garantie der MARKTREIFE
oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK. Details finden Sie in der
GNU Affero General Public License.

Sie sollten ein Exemplar der GNU Affero General Public License zusammen mit diesem
Programm erhalten haben. Falls nicht, siehe <http://www.gnu.org/licenses/>.
```
